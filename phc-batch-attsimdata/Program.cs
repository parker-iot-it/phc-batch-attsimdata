﻿using System;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Data;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace phc_batch_attsimdata
{
    class Program
    {
        static IConfigurationRoot _configuration;
        static string _logFileName = "";
        static string _errorMessages = "";
        static string _cycleDate = "";
        static Int64 _counter = 0;

        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            try
            {
                _logFileName = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\LOGS\\attSimData_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
                WriteToLog("STARTING AT&T SIM DATA");

                //Delete log files older than 1 months
                WriteToLog("DELETING LOG FILES OLDER THAN 1 MONTH FROM " + Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\LOGS");
                string[] files = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\LOGS");
                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.LastAccessTime < DateTime.Now.AddMonths(-1)) fi.Delete();
                }

                var builder = new ConfigurationBuilder()
                    .SetBasePath(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location))
                    .AddJsonFile("appSettings.json", optional: true, reloadOnChange: true);
                _configuration = builder.Build();

                if (DateTime.Now.Month == 1) _cycleDate = (DateTime.Now.Year - 1).ToString() + "-12-19";
                else _cycleDate = DateTime.Now.Year.ToString() + "-" + (DateTime.Now.Month - 1).ToString("00") + "-19";

                WriteToLog("CYCLE DATE: " + _cycleDate);

                using (SqlConnection cnn = new SqlConnection(_configuration["DeviceDbConnString"]))
                {
                    cnn.Open();
                    string sql = "";
                    List<string> sims = new List<string>();

                    //Remove any existing records taht exist for this cycle date
                    WriteToLog("REMOVING ANY EXISTING RECORDS FOR CYCLE DATE");
                    sql = "DELETE [dbo].[AttSimData] WHERE [CycleStartDate] = @cyclestartdt";
                    using (var cmd = new SqlCommand(sql, cnn))
                    {
                        cmd.Parameters.Add("@cyclestartdt", SqlDbType.DateTime).Value = Convert.ToDateTime(_cycleDate);
                        int numberOfRecords = cmd.ExecuteNonQuery();
                        WriteToLog("DELETED RECORDS: " + numberOfRecords);
                    }

                    //Get distinct list of sims from Device Table
                    WriteToLog("GETTING DISTINCT LIST OF SIMS FROM DEVICE TABLE");
                    sql = "SELECT DISTINCT(SimIccId) FROM [dbo].[MasterTags] WHERE SimIccId IS NOT NULL ORDER BY SimIccId";
                    using (SqlCommand cmd = new SqlCommand(sql, cnn))
                    using (SqlDataReader dr = cmd.ExecuteReader())
                        while (dr.Read()) { sims.Add(dr.GetString(0)); }

                    WriteToLog("TOTAL SIMS: " + sims.Count);

                    int concurrentSims = Convert.ToInt16(_configuration["ConcurrentSims"]);

                    decimal loopCount = Math.Ceiling(Convert.ToDecimal(Convert.ToDouble(sims.Count) / concurrentSims));

                    WriteToLog("CONCURRENT SIMS: " + concurrentSims + "   TOTAL RUNS: " + loopCount);

                    for (var i = 0; i < loopCount; i++)
                    {
                        List<string> runList = new List<string>();
                        for (var j = 0; j < concurrentSims; j++)
                            if (((i * concurrentSims) + j) < sims.Count) runList.Add(sims[(i * concurrentSims) + j]);

                        IEnumerable<Task<string>> downloadTasksQuery = from sim in runList select ProcessSim(sim, cnn);
                        List<Task<string>> downloadTasks = downloadTasksQuery.ToList();

                        while (downloadTasks.Count > 0)
                        {
                            // Identify the first task that completes.  
                            Task<string> firstFinishedTask = await Task.WhenAny(downloadTasks);

                            // ***Remove the selected task from the list so that you don't process it more than once.  
                            downloadTasks.Remove(firstFinishedTask);

                            // Await the completed task.  
                            string tmp = await firstFinishedTask;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _errorMessages = _errorMessages + Environment.NewLine + "(Main) " + ex.Message;
                WriteToLog("ERROR: " + ex.Message);
            }
            finally
            {
                string subject = "COMPLETED - AT&T SIM DATA: " + _cycleDate;
                string body = "Application finished successfully.  Total Sims Processed: " + _counter;

                if (_errorMessages.Trim() != "")
                {
                    body = "Errors occurred while running the at&t monthly sim data job." + 
                            Environment.NewLine + Environment.NewLine +
                            "LOG FILE: " + _logFileName +
                            Environment.NewLine +
                            _errorMessages;
                    subject = "ERRORS - AT&T SIM DATA: " + _cycleDate;
                } 

                using (MailMessage mail = new MailMessage("parkerit_iot@parker.com", _configuration["ErrorEmailToAddr"]))
                    using (SmtpClient client = new SmtpClient())
                    {
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Host = "cor-smtp1.us.parker.corp";
                        mail.Subject = "COMPLETED - AT&T SIM DATA: " + _cycleDate;
                        mail.Body = body;
                        client.Send(mail);
                    }

                WriteToLog("AT&T SIM DATA FINISHED");
            }
        }

        //Private Methods
        private static async Task<string> ProcessSim(string sim, SqlConnection cnn)
        {
            int tryCount = 0;
            while (tryCount < 3 ) {
                try
                {
                    tryCount++;
                    using (HttpClient client = new HttpClient { BaseAddress = new Uri(_configuration["ATTEndpoint"]) })
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _configuration["ATTApiKey"]);

                        long dataUsage = 0;
                        string url = "/rws/api/v1/devices/" + sim + "/usageInZone?cycleStartDate=" + _cycleDate;

                        WriteToLog("STARTING SIM: " + sim + "  URL: " + url);
                        var response = await client.GetAsync(url);
                        if (response.IsSuccessStatusCode)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            JToken token2 = JToken.Parse(result.Result).SelectToken("deviceCycleUsageInZones");
                            foreach (JToken z in token2)
                                foreach (JToken jt in z)
                                    foreach (JToken s in (JArray)jt)
                                        dataUsage = dataUsage + (long)s["dataUsage"];
                        }
                        else
                        {
                            string msg = "ERROR CALLING AT&T: Sim: " + sim + " - " + response.StatusCode.ToString() + ": " + response.ToString();

                            if (response.StatusCode == System.Net.HttpStatusCode.ServiceUnavailable ||
                                response.StatusCode == System.Net.HttpStatusCode.ProxyAuthenticationRequired) throw new Exception(msg);

                            if (response.StatusCode != System.Net.HttpStatusCode.NotFound)
                            {
                                dataUsage = -1;
                                _errorMessages = _errorMessages + Environment.NewLine + msg;
                                WriteToLog(msg);
                            }
                        }

                        //INSERT DATA RECORD
                        if (dataUsage >= 0)
                        {
                            string sql = "INSERT INTO [dbo].[AttSimData] (SimIccid, CycleStartDate, Bytes ) VALUES (@simiccid, @cyclestartdt, @bytes)";
                            using (var insCommand = new SqlCommand(sql, cnn))
                            {
                                WriteToLog(sim + "   USAGE: " + dataUsage);
                                insCommand.Parameters.Add("@simiccid", SqlDbType.VarChar).Value = sim;
                                insCommand.Parameters.Add("@cyclestartdt", SqlDbType.DateTime).Value = Convert.ToDateTime(_cycleDate);
                                insCommand.Parameters.Add("@bytes", SqlDbType.BigInt).Value = dataUsage;
                                insCommand.ExecuteNonQuery();
                            }
                        }
                        _counter++;
                        WriteToLog("PROCESSED: " + _counter);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    WriteToLog("ERROR IN ProcessSim (ATTEMPT " + tryCount + "): " + sim + " - " + ex.Message);
                    if (tryCount >= 3) throw ex;
                }
            }

            return "";
        }
        private static void WriteToLog(string msg)
        {
            try
            {
                Console.WriteLine(DateTime.Now.ToString() + ": " + msg);
                msg = string.Format("{0} " + msg + Environment.NewLine, DateTime.Now.ToString() + ": ");

                int retryCounter = 0;
                while (retryCounter < 3)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(_logFileName);
                        File.AppendAllText(_logFileName, msg);
                        fi = null;
                        break;
                    }
                    catch { retryCounter++; }
                }
            } catch { }
        }
    }
}
